#
# crawl a long list of URLs (>1 million)
# The idea is to partition the long list into subsets with each st
# containing a certain number of URLs (in config.py)
#
import config
import sys
import os
import subprocess

def partition_urllistfile():
    # load URL list file
    if not os.path.exists(config.urllistfile):
        sys.exit("config.urllistfile not found: "+config.urllistfile)
    filepath,filename = os.path.split(config.urllistfile)
    trunk,extension = os.path.splitext(filename)

    # write out the list file for each batch job first
    li = 0
    writting = False
    listdir = os.path.join(config.writepath,config.listdir)
    if not os.path.exists(listdir):
        os.makedirs(listdir)

    batchlistfiles = []
    print "loading urllistfile..."
    with open(config.urllistfile) as f:
        for line in f:
            # close existing files and 
            # create a new batch job list
            if (li % config.batch_size) == 0:
                 filenamew = trunk+"-"+str(li/config.batch_size)+extension
                  
                 filepathw = os.path.join(listdir,filenamew)
                 if "fw" in locals() and (not fw.close):
                     fw.close()
                     
                 fw = open(filepathw,'w')
                 batchlistfiles.append(fw.name)
                 print 'new file created: ',fw.name

            fw.write(line)
            li += 1

    if not fw.close:
        fw.close()
    
    print "%(n)d batch list files written to: %(listdir)s" % {"n":len(batchlistfiles),"listdir":listdir}
   
    return batchlistfiles

def run_crawlermk(batchlistfiles):
    logdir = os.path.join(config.writepath,config.logdir)
    datadir = os.path.join(config.writepath,config.datadir)
    if not os.path.exists(logdir):
        os.makedirs(logdir)
    if not os.path.exists(datadir):
        os.makedirs(datadir)

    for bf in batchlistfiles:
        # generate log file names
        listfilename = os.path.split(bf)[1]
        listfiletrunk = os.path.splitext(listfilename)[0]
        logfilename = listfiletrunk+'.log'
        logfilepath = os.path.join(logdir,logfilename)

        # create the data downloading directory
        downloaddir = os.path.join(datadir,listfiletrunk)
        if not os.path.exists(downloaddir):
            os.makedirs(downloaddir)
        cmd = 'cat %(urls_file)s | parallel -P %(crawlers)s %(cmd)s "{}" %(writepath)s' %\
             {"urls_file":bf,"crawlers":config.crawlers,\
              "cmd":"python crawler.py","writepath":downloaddir}

        # run the downloading script data
        print 'cmd = ',cmd
        p = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True)
        (output,err) = p.communicate()
        
        # write output into the log file
        flog = open(logfilepath,'w')
        print 'output=',output
        flog.write(output)
        if err:
            flog.write(err)
        flog.close()

    print "documents downloaded to: ",datadir
        

if __name__ == "__main__":
    
    # partition original URL list file into smaller ones
    batchlistfiles = partition_urllistfile()
                 
    # run crawlermk 
    run_crawlermk(batchlistfiles)

