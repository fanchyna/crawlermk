import sys, os 
import urllib2
import urllib
import time
import random
import datetime
import traceback
import url_normalization
import signal
import hashlib
import urlparse

def handler(signum,frame):
    print "file downloading too slow"
    raise Exception("skip")

user_agent = 'Mozilla/5.0 (compatible; citeseerxbot/2.0; +http://citeseerx.ist.psu.edu)'
values = {'name' : 'M',
          'location' : 'US',
          'language' : 'en',
	  'User-Agent':user_agent}


signal.signal(signal.SIGALRM, handler)
signal.alarm(600)

def crawl(url,timeout=60, maxtries=1):
	try:
		for i in range(maxtries):
			#data = urllib.urlencode(values)
			req = urllib2.Request(url_normalization.get_canonical_url(url), headers=values)
			response = urllib2.urlopen(req,timeout=timeout)
			the_page = response.read()
			fetched_url = response.geturl()
			headers =  response.info()
			return fetched_url , headers, the_page
		
	except Exception as ex:
		pass
		#print ex
		#traceback.print_exc()
		
# derive the 2-level hex path given a string
# the function returns a path like root/01/3f/
def derive_path(string,root):
    h=hashlib.md5(string).hexdigest()
    subdir1 = h[0:2]
    subdir2 = h[2:4]
    l = [root,subdir1,subdir2]
    filedir = os.path.join(*l)
    # create the dir if it does not exit
    if not os.path.exists(filedir):
        os.makedirs(filedir)

    return os.path.join(filedir)
    
		
def write(called_url,fetched_url,headers, the_page, folder_path):
	try:
		if headers.gettype().lower() == "application/pdf":
			timestamp = timeStamped(str(random.randint(1,1000)))
                        filedir = derive_path(timestamp,folder_path)
                        
			f=open(os.path.join(filedir,timestamp+'.pdf'),'wb')
			f.write(the_page)
			f.close()

			f=open(os.path.join(filedir,timestamp+'.txt'),'w')
			f.write('URL: '+ called_url + '\n')
			f.write('Fetched: ' + fetched_url)
			f.close()
		else:
			print "url %s ignored because mime type %s" %(called_url,headers.gettype())
	except Exception as ex:
		pass
		#print ex
		#traceback.print_exc()
		
def timeStamped(fname, fmt='%Y-%m-%d-%H-%M-%S_{fname}'):
	return datetime.datetime.now().strftime(fmt).format(fname=fname)
		
def in_blacklist(url):
    blacklist = []
    domain_blacklist = ["asme.org"]

    scheme,host,path,query,fragment = urlparse.urlsplit(url)
    # check blacklist items
    for bl in blacklist:
        if url.replace(scheme+"://","").startswith(bl):
            return True
    # check domain blacklist item
    for dbl in domain_blacklist:
        if host.endswith("."+dbl):
            return True
    return False


# usage:
# $ python crawler.py [url] [folder_path] 
if __name__=='__main__':
	if len(sys.argv) > 1:
		try:
			url = sys.argv[1]
			folder_path = sys.argv[2]
			if not os.path.exists(folder_path):
			 	os.path.makedirs(folder_path)
                        if not in_blacklist(url):
			    fetched_url , headers, the_page = crawl(url)
                        else: 
                            print "in blacklist: %s" %(url)
                            raise ValueError("url is in blacklist")
			if the_page != None:
				write(url,fetched_url,headers, the_page, folder_path)
		except Exception as ex:
			print "Failed with %s" %(url)
			#traceback.print_exc()
	
