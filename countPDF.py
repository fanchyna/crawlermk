#
# count the number of PDF files downloaded 
#
import os
import sys
import config
import subprocess

def main():
    # construct the full output directory 
    full_download_dir = os.path.join(config.writepath,config.datadir)
    # run the "find" command to count the number of files
    cmd = "find "+full_download_dir+" -iname '*.pdf' |wc -l"
    p = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True)
    (output,err) = p.communicate()
    print "#PDF downloaded: ",output

if __name__=="__main__":
    main()
