#
# Crawl a number of URL list files 
#
from long_list_crawl import run_crawlermk
import glob
import sys
def read_in(prompt=""):
    print prompt
    stdinput = sys.stdin.readline()
    response = 'n'
    if stdinput == "\n":
       response = 'Y'
    else:
       # remove the last "\n"
       stdinput_bare = stdinput.strip("\n")
       if stdinput_bare == 'Y' or stdinput_bare == 'n':
           response = stdinput_bare
       else:
           sys.exit("Invalid input. Type Y or n.")
    if response == "Y": return True
    if response == "n": return False
        

#
# find URL list files by interpretating a regular expression 
def find_urllistfile(filepath):
    filepaths = sorted(glob.glob(filepath))
    for fp in filepaths:
        print fp
    print 'number of files: ',len(filepaths)
    return filepaths

if __name__ == "__main__":
    if len(sys.argv) != 2: 
        print "usage: python partitioned_list_crawl.py [file wildcard]"
    else:
        filepath = str(sys.argv[1])
        urllistfiles = find_urllistfile(filepath)
        response = read_in(prompt="Crawl the file list above? [Y]/n")
        if response:
            # see config.py for configurations in run_crawlermk
            run_crawlermk(urllistfiles)
        else:
           print "program quitted."
        
