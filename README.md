CRAWLERMK

* Overview
  Crawlermk is a multiple thread Python crawler initially written by Madian Khabsa. Dr. Jian Wu wrote a wrapper so it can crawl long list of URLs. The crawler allow users to specify the number of threads ("crawlers" in config.py). It currently only supports application/pdf formats as this was what it was designed to do. 

* Usage
  You need Python2.6+ and GNU parallel installed to run this crawler. 
  After pulling the original master branch, copy config-template.py to config.py. 

  To run a short list of URLs (normally less than the "batch_size" parameter in config.py), you do not need to split the list into multiple lists and all files are saved under a single directory. This can be done by 
  (1) edit crawl.sh to specify the output directory and create it manually. 
  (2) Run the following command:
      $ ./crawl.sh urlfile.txt <num-procs> 

  If you are running a long list of URLs, you do not want to save more than 10000 files under a single directory, so you want to split the long list into several smaller ones each containing a fixed number of URLs (specified in "batch_size" in config.py). 
  (1) edit config.py to specify "urllistfile", "crawlers", "writepath" and "batch_size". You do not need to create the output directories manually. Just make sure you have permission to write into it. 
  (2) run the following command:
      $ ./run_long_list_crawl.sh 
  This will output the splitted urllist into "writepath/listdir", failed URLs to "writepath/logdir", and download files to "writepath/datadir". 

  If you already have partitioned URL list files (this occurs when a long list crawl process is stopped but not finished), you can use partition_list_crawl.py. You just need to input the filenames (wildcard supported, such as "*" or "[0-9]")
  (1) Edit config.py, to specify the "crawlers", "writepath". You do not need to edit the "urllistfile" and "batch_size" because they are only used by "long_list_crawl.py".
  (2) python partition_list_crawl.py [partitioned URL list files]
  e.g., $ python partition_list_crawl.py '/data02/mas-pdf-download-dwj/seeds_partition/mas-paper_url-dwj-data1-part2r-accepted-14[1-9].txt'

* Note:
  If you are crawling partitioned URL list files copied from another server, make sure that their file names do not overlap with the existing URL list file names. This ensures that the downloaded files are written to different directories from existing directories under the download directory. 

  
* Testing
  The package provides a short and a long test seed URLs, saved at 
  * test/urllist-test-short.txt and 
  * test/urllist-test-long.txt.


