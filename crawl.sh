#!/bin/bash
# a basic crawler in bash
# usage: crawl.sh urlfile.txt <num-procs> writepath
URLS_FILE=$1
CRAWLERS=$2
WRITEPATH=$3

CMD="python crawler.py"

# print the starting datetime
date
cat $URLS_FILE | parallel -P $CRAWLERS $CMD "{}" $WRITEPATH
# print the ending datetime
date
