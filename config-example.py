#
# configuration file for long_list_crawl
#
#####################
# 
# URL list file path
#urllistfile = "urllist-test.txt"
urllistfile = "/data02/mas-pdf-download-dwj/seeds/mas-paper_url-dwj-data1-part1r-accepted.txt"
# Number of processes (threads) used
crawlers = 48
# Top level folder of download directory
writepath = "/data02/mas-pdf-download-dwj/"
#writepath = "./download/"
# subfolder to store URL list files
listdir = "seeds_partition/"
# subfolder to store log files
logdir = "failed_log/"
# subfolder to store downloaded data (including a .pdf and .txt)
datadir = "download/"
# number of URLs in a batch (10 for testing, typically 10000)
batch_size = 10000
