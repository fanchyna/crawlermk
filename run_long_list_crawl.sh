#!/bin/bash
START=$(date +%s)
STARTH=$(date)
BATCHID=`date +%Y-%m-%d`

# do something
# start your script work here
python long_list_crawl.py
 
# your logic ends here
END=$(date +%s)
ENDH=$(date)
DIFF=$(( $END - $START ))
echo "Job ID: $BATCHID"
echo "Started at: $STARTH. "
echo "  Ended at: $ENDH." 
echo "  Job took: $DIFF seconds."

